const { getInstance } = require('../src/CoteSingleton');
const cote = require('cote')

test("getInstance should return a valid instance", done => {

    global.console = { log: jest.fn() }

    const publisher = getInstance({ type: "publisher", key: "user" });
    const publisher_room = getInstance({ type: "publisher", key: "room" });
    const subscriber = getInstance({ type: "subscriber", key: "user" });
    const requester = getInstance({ type: "requester", key: "user" });
    const responder = getInstance({ type: "responder", key: "user" });

    expect(getInstance({ type: "publisher", key: "user" })).toBeInstanceOf(cote.Publisher);
    expect(getInstance({ type: "publisher", key: "room" })).toBeInstanceOf(cote.Publisher);
    expect(getInstance({ type: "subscriber", key: "user" })).toBeInstanceOf(cote.Subscriber);
    expect(getInstance({ type: "requester", key: "user" })).toBeInstanceOf(cote.Requester);
    expect(getInstance({ type: "responder", key: "user" })).toBeInstanceOf(cote.Responder);
    done()
});