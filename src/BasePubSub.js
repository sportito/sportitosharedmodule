const { environment, microservice } = require('../config');
const { getInstance } = require('./CoteSingleton');
const { log } = require('./Logger');

class BasePubSub {
    constructor(event) {
        const eventArray = event.split('.');

        if (eventArray.length != 2) throw new Error('event not well formed');

        this.key = eventArray[0];
        this.event = eventArray[1];

        this.subscribe = this.subscribe.bind(this);
        this.publish = this.publish.bind(this);
    }

    publish(message) {
        const publisher = getInstance({type: 'publisher', key:this.key});
        console.log({ type: 'publisher', key: this.key});
        publisher.publish(this.event, message);
        log({environment, microservice, event: this.event, key: this.key, message});
    }

    subscribe(func) {
        const subscriber = getInstance({type: 'subscriber', key:this.key});
        console.log({ type: 'subscriber', key: this.key});
        subscriber.on(this.event, func);
    }
}

module.exports = BasePubSub;