const { environment, microservice } = require('../config');
const cote = require('cote')({ environment });

const instances = {
    requester: {},
    responder: {},
    publisher: {},
    subscriber: {}
};

const initInstance = ({type, key}) => {
    const params = {
        name: `${microservice} ${type}`,
        key
    };

    switch (type) {
        case 'publisher':
            instances[type][key] = new cote.Publisher(params);
            break;
        case 'subscriber':
            instances[type][key] = new cote.Subscriber(params);
            break;
        case 'requester':
            instances[type][key] = new cote.Requester(params);
            break;
        case 'responder':
            instances[type][key] = new cote.Responder(params);
            break;
        default: throw new Error('type not allowed')
    }
};

const getInstance = ({type, key}) => {
    let pool = {};

    switch (type) {
        case 'publisher':
        case 'subscriber':
        case 'requester':
        case 'responder':
            pool = instances[type];
            break;
        default: throw new Error('type not allowed')
    }

    if (!pool[key]) initInstance({type, key});

    return pool[key];
};

module.exports = { getInstance };