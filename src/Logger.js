const MongoClient = require('mongodb').MongoClient;
const config = require('../config/index');

let db = null;

const connection = async () => {
    if(db == null) {
        const client = await MongoClient.connect(config.logger_connection, { useNewUrlParser: true });
        db = await client.db(config.logger_database);
    }

    return db;
};

const log = async ({environment, microservice, event, key, message}) => {
    try {
        const conn = await connection();
        await conn.collection(config.logger_collection).insertOne({environment, microservice, event, key, message, date: new Date()});
    } catch (e) {
        console.error('COTE LOGGER CONNECTION NOT AVAILABLE');
    }
};

module.exports = {
    log
};