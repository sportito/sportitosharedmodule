const BasePubSub = require('./BasePubSub');
const { PUBSUB_EVENT } = require('../config/eventsKeyMap');

class DEPOSIT_SUCCESS extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.DEPOSIT_SUCCESS);
    }
}

class DEPOSIT_FAIL extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.DEPOSIT_FAIL);
    }
}

class ROOM_JOINED extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.ROOM_JOINED);
    }
}

class PASSWORD_CHANGED extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.PASSWORD_CHANGED);
    }
}

class RESET_PASSWORD extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.RESET_PASSWORD);
    }
}

class WITHDRAWAL_REQUESTED extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.WITHDRAWAL_REQUESTED);
    }
}

class WITHDRAWAL_CONFIRMED extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.WITHDRAWAL_CONFIRMED);
    }
}

class WITHDRAWAL_DENIED extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.WITHDRAWAL_DENIED);
    }
}

class MANUALLY_BONUS_ASSIGNED extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.MANUALLY_BONUS_ASSIGNED);
    }
}

class ACCOUNT_VERIFIED extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.ACCOUNT_VERIFIED);
    }
}

class LEADERBOARD_GENERATED extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.LEADERBOARD_GENERATED);
    }
}

class ROOMS_RESULTS extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.ROOMS_RESULTS);
    }
}

class USER_REGISTERED extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.USER_REGISTERED);
    }
}

class USER_REGISTERED_ALL extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.USER_REGISTERED_ALL);
    }
}

class INVITE_FRIEND extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.INVITE_FRIEND);
    }
}

class EXCLUSIVE_CONTEST extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.EXCLUSIVE_CONTEST);
    }
}

class EMAIL_CUSTOM extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.EMAIL_CUSTOM);
    }
}

class EMAIL_DOCUMENT extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.EMAIL_DOCUMENT);
    }
}

class PLAYERSTATISTICS_RUN extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.PLAYERSTATISTICS_RUN);
    }
}

class SNEAK_MATCHES extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.SNEAK_MATCHES);
    }
}

class MISSING_PLAYER_RUN extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.MISSING_PLAYER_RUN);
    }
}

class CREATE_ROOM extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.CREATE_ROOM);
    }
}

class STARTING_LINEUPS extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.STARTING_LINEUPS);
    }
}

class DOCUMENT_VALIDATED extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.DOCUMENT_VALIDATED);
    }
}

class DOCUMENT_DELETED extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.DOCUMENT_DELETED);
    }
}

class DOCUMENT_PROX_EXPIRED extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.DOCUMENT_PROX_EXPIRED);
    }
}

class DOCUMENT_EXPIRED extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.DOCUMENT_EXPIRED);
    }
}

class USER_SUSPENDED extends BasePubSub {
    constructor() {
        super(PUBSUB_EVENT.USER_SUSPENDED);
    }
}

module.exports = {
    DEPOSIT_SUCCESS,
    DEPOSIT_FAIL,
    ROOM_JOINED,
    PASSWORD_CHANGED,
    RESET_PASSWORD,
    WITHDRAWAL_REQUESTED,
    WITHDRAWAL_CONFIRMED,
    WITHDRAWAL_DENIED,
    ACCOUNT_VERIFIED,
    MANUALLY_BONUS_ASSIGNED,
    LEADERBOARD_GENERATED,
    ROOMS_RESULTS,
    USER_REGISTERED,
    USER_REGISTERED_ALL,
    INVITE_FRIEND,
    EXCLUSIVE_CONTEST,
    EMAIL_CUSTOM,
    EMAIL_DOCUMENT,
    PLAYERSTATISTICS_RUN,
    SNEAK_MATCHES,
    MISSING_PLAYER_RUN,
    CREATE_ROOM,
    STARTING_LINEUPS,
    DOCUMENT_VALIDATED,
    DOCUMENT_DELETED,
    DOCUMENT_PROX_EXPIRED,
    DOCUMENT_EXPIRED,
    USER_SUSPENDED
};