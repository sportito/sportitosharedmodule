const { getInstance } = require('./CoteSingleton');

class BaseRPC {
    constructor(event) {
        const eventArray = event.split('.');

        if (eventArray.length != 2) throw new Error('event not well formed');

        this.key = eventArray[0];
        this.event = eventArray[1];

        this.request = this.request.bind(this);
        this.respond = this.respond.bind(this);
    }

    async request(message) {
        const sender = getInstance({ type: 'requester', key: this.key });
        console.log({ type: 'requester', key: this.key});
        return await sender.send({ type: this.event, ...message });
    }

    respond(func) {
        const responder = getInstance({ type: 'responder', key: this.key });
        responder.on(this.event, (message) => {
            console.log({ type: 'responder', key: this.key});
            delete message.type;
            return func(message)
        });
    }
}

module.exports = BaseRPC;