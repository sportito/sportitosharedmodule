const BaseRpc = require('./BaseRPC');
const { RPC_EVENT } = require('../config/eventsKeyMap');

class GET_USER_PROFILE extends BaseRpc {
    constructor() {
        super(RPC_EVENT.GET_USER_PROFILE);
    }

    /**
     * 
     * @param { ids, decorateWithStatistics = false } message 
     */
    async request(message) {
        return super.request(message)
    }
}

class GET_LEADERBOARD extends BaseRpc {
    constructor() {
        super(RPC_EVENT.GET_LEADERBOARD);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class GET_TRANSACTIONS extends BaseRpc {
    constructor() {
        super(RPC_EVENT.GET_TRANSACTIONS);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class GET_TRANSACTIONS_BY_USER extends BaseRpc {
    constructor() {
        super(RPC_EVENT.GET_TRANSACTIONS_BY_USER);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class GET_BONUS extends BaseRpc {
    constructor() {
        super(RPC_EVENT.GET_BONUS);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class GET_ROOM extends BaseRpc {
    constructor() {
        super(RPC_EVENT.GET_ROOM);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class GET_SPECIAL_ROOMS extends BaseRpc {
    constructor() {
        super(RPC_EVENT.GET_SPECIAL_ROOMS);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class TEAMS extends BaseRpc {
    constructor() {
        super(RPC_EVENT.TEAMS);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class TOURNAMENTS extends BaseRpc {
    constructor() {
        super(RPC_EVENT.TOURNAMENTS);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class MATCHES extends BaseRpc {
    constructor() {
        super(RPC_EVENT.MATCHES);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class MATCH_BY_ID extends BaseRpc {
    constructor() {
        super(RPC_EVENT.MATCH_BY_ID);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class SOCCER_TEAM extends BaseRpc {
    constructor() {
        super(RPC_EVENT.SOCCER_TEAM);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class SOCCER_MATCH_TIMELINE extends BaseRpc {
    constructor() {
        super(RPC_EVENT.SOCCER_MATCH_TIMELINE);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class MATCH_LINEUP extends BaseRpc {
    constructor() {
        super(RPC_EVENT.MATCH_LINEUP);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class SOCCER_ALL_TOURNAMENT extends BaseRpc {
    constructor() {
        super(RPC_EVENT.SOCCER_ALL_TOURNAMENT);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class SOCCER_TOURNAMENT_SCHEDULE extends BaseRpc {
    constructor() {
        super(RPC_EVENT.SOCCER_TOURNAMENT_SCHEDULE);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class SOCCER_MISSING_PLAYER extends BaseRpc {
    constructor() {
        super(RPC_EVENT.SOCCER_MISSING_PLAYER);
    }

    /**
     *
     * @param { id } message
     */
    async request(message) {
        return super.request(message)
    }
}

class GET_EMAIL_TEMPLATE extends BaseRpc {
    constructor() {
        super(RPC_EVENT.GET_EMAIL_TEMPLATE);
    }

    /**
     *
     * @param { id } message
     */

    async request(message) {
        return super.request(message)
    }
}

class GET_USER_DOCUMENT extends BaseRpc {
    constructor() {
        super(RPC_EVENT.GET_USER_DOCUMENT);
    }

    /**
     *
     * @param { id } message
     */

     async request(message) {
         return super.request(message);
     }
}

class DEPOSIT_FAIL_MAIL extends BaseRpc {
    constructor() {
        super(RPC_EVENT.DEPOSIT_FAIL_MAIL);
    }

    /**
     *
     * @param { id } message
     */

     async request(message) {
         return super.request(message);
     }
}

class GET_GAMBLERS extends BaseRpc {
    constructor() {
        super(RPC_EVENT.GET_GAMBLERS);
    }

    /**
     *
     * @param { ids, decorateWithStatistics = false } message
     */
    async request(message) {
        return super.request(message)
    }
}


module.exports = {
    GET_USER_PROFILE,
    GET_LEADERBOARD,
    GET_TRANSACTIONS,
    GET_TRANSACTIONS_BY_USER,
    GET_ROOM,
    GET_BONUS,
    GET_SPECIAL_ROOMS,
    TEAMS,
    TOURNAMENTS,
    MATCHES,
    MATCH_BY_ID,
    SOCCER_TEAM,
    SOCCER_MATCH_TIMELINE,
    MATCH_LINEUP,
    SOCCER_ALL_TOURNAMENT,
    SOCCER_TOURNAMENT_SCHEDULE,
    SOCCER_MISSING_PLAYER,
    GET_EMAIL_TEMPLATE,
    GET_USER_DOCUMENT,
    DEPOSIT_FAIL_MAIL,
    GET_GAMBLERS
}