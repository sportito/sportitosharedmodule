module.exports = {
    environment: process.env.NODE_ENV || 'dev' + process.env.USER,
    microservice: process.env.MICROSERVICE || '',
    logger_connection: process.env.MONGO_LOGGER_CONNECTION,
    logger_database: 'EventStore',
    logger_collection: 'events'
};