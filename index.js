const PubSubEvent = require('./src/PubSubEvent');
const RPCEvent = require('./src/RPCEvent');

console.log('COTE.JS SETTINGS:', require('./config/index'));

module.exports = {
    subscriberEvents: { ...PubSubEvent },
    rpcEvents: { ...RPCEvent }
};
