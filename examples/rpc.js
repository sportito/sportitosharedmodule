const { GET_USER_PROFILE } = require('../index').rpcEvents;

new GET_USER_PROFILE().request(1).then(console.log);

const handleResponse = async req => {
    req.received = true;
    return req;
}

setTimeout(() => {
    new GET_USER_PROFILE().respond(handleResponse);
}, 2000);
