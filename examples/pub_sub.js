const {DEPOSIT_SUCCESS} = require('../index');

setTimeout(() => {
    new DEPOSIT_SUCCESS().subscribe((req) => {
        console.log('sub1', req);
    });

    new DEPOSIT_SUCCESS().subscribe((req) => {
        console.log('sub2', req);
    });

    new DEPOSIT_SUCCESS().subscribe((req) => {
        console.log('sub3', req);
    });
},5000);

new DEPOSIT_SUCCESS().publish({
        nome: 'Giovanni 1',
        cognome: 'Scarioli 1'
});


new DEPOSIT_SUCCESS().publish({
        nome: 'Giovanni 2',
        cognome: 'Scarioli 2'
});
